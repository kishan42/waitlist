import Home from "./components/Home";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import CustomerMain from "./components/Customer/CustomerMain";
import WaitingListMain from "./components/WaitingList/WaitingListMain";
import AssignmentsMain from "./components/Assignments/AssignmentsMain";
import AssignmentsForm from "./components/Assignments/AssignmentsForm";
import AssignmentList from "./components/Assignments/AssignmentList";
import WaitListTable from "./components/WaitingList/WaitListTable";
import WaitListForm from "./components/WaitingList/WaitListForm";

export const routes = [
    {
        path: '/',
        component: Home,
    },
    {
        path:'/waiting/:restaurantId/:waitingId',
        component:Home,
    },
    {
        path: '/login',
        component: Login,
    },
    {
        path: '/register',
        component: Register
    },
    {
        path: '/customers',
        component: CustomerMain,
    },
    {
        path: '/waiting-list',
        meta: {
            requiresAuth: true,
        },
        component: WaitingListMain,

        children: [
            {
                path: '/',
                component: WaitListTable
            },
            {
                path:'history',
                // component:
            },
            {
                path: 'new',
                component: WaitListForm
            },
            {
                path: 'edit/:id',
                component: WaitListForm
            },
        ],
    },
    {
        path: '/assignments',
        meta: {
            requiresAuth: true,
        },
        component: AssignmentsMain,
        children: [
            {
                path: '',
                component: AssignmentList
            },
            {
                path: 'new',
                component: AssignmentsForm
            },
            {
                path: 'edit/:id',
                component: AssignmentsForm
            },
        ],
    },
];
