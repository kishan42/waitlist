import {getLocalUser} from "./helpers/auth";
import moment from "moment";
import {getLocalRestaurantId} from "./helpers/restaurant";
import {getLocalWaitingId} from "./helpers/waitList";

const user = getLocalUser();
const restaurantId = getLocalRestaurantId();
const waitingId = getLocalWaitingId();

export default {
    state: {
        currentUser: user,
        isLoggedIn: !!user,
        loading: false,
        authError: null,
        registerError: null,
        message: null,
        assignments: [],
        restaurantId: restaurantId,
        waitingId: waitingId,
    },
    getters: {
        isLoggedIn(state) {
            return state.isLoggedIn;
        },
        isLoading(state) {
            return state.loading;
        },
        currentUser(state) {
            return state.currentUser;
        },
        authError(state) {
            return state.authError;
        },
        registerError(state) {
            return state.registerError;
        },
        restaurantId(state) {
            return state.restaurantId;
        },
        waitingId(state) {
            return state.waitingId;
        },
    },
    mutations: {
        login(state) {
            state.loading = true;
        },
        register(state) {
            state.loading = true;
        },

        loginSuccess(state, payload) {
            state.authError = null;
            state.isLoggedIn = true;
            state.loading = false;
            let token_expires = moment().add(payload.expires_in, 'seconds').unix();
            state.currentUser = Object.assign({}, payload.user, {
                token: payload.access_token,
                token_expiry: token_expires
            });
            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        loginFailed(state, payload) {
            state.loading = false;
            state.authError = payload.error;
            console.log(payload.error);
        },

        registerSuccess(state, payload) {
            state.authError = null;
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});
            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        registerFailed(state, payload) {
            state.loading = false;
            state.registerError = payload.error;
        },

        logout(state) {
            localStorage.removeItem("user");
            state.isLoggedIn = false;
            state.currentUser = null;
        },
        setRestaurantId(state,restaurantId) {
            state.restaurantId = restaurantId;
            localStorage.setItem("restaurantId", JSON.stringify(state.restaurantId));
        },
        setWaitingId(state,waitingId) {
            state.waitingId = waitingId;
            localStorage.setItem("waitingId", JSON.stringify(state.waitingId));
        },
        addAssignments(state, assignments) {
            state.assignments = Object.assign({}, assignments);
        },
    },
    actions: {
        login(context) {
            context.commit("login");
        },
        register(context) {
            context.commit("register");
        }
    }
}
