import moment from "moment";

require('./bootstrap');

import Vue from "vue";
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import {routes} from './routes'
import StoreData from './store'
import MainApp from './components/MainApp.vue'
import {logout} from "./helpers/auth";

window.axios = require('axios');

Vue.use(VueRouter);
Vue.use(Vuex);

const store = new Vuex.Store(StoreData);

window.axios.interceptors.request.use((config) => {
    const currentUser = store.getters.currentUser;
    if (currentUser && currentUser.token) {
        config.headers.authorization = "Bearer " + currentUser.token;
    }
    return config;
});

window.axios.interceptors.response.use(null, (error) => {
    if (error.response.status == 401) {
        store.commit('logout');
        router.push('login');
    }else{
        return Promise.reject(error);
    }
});

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const router = new VueRouter({
    routes,
    mode: 'history'
});


router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const currentUser = store.state.currentUser;
    if (requiresAuth && !currentUser) {
        next('/login')
    } else if (to.path == '/login' && currentUser) {
        next('/');
    } else {
        if (currentUser && currentUser.token_expiry < moment().unix()) {
            logout(store, router);
        } else {
            next();
        }
    }
});

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        MainApp
    }
});
