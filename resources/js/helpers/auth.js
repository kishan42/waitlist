export function login(credentials) {
    return new Promise((res, rej) => {
        axios.post("/api/auth/login", credentials)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej("invalid email or password");
            });
    })
}

export function register(credentials) {
    return new Promise((res, rej) => {
        axios.post("/api/auth/register", credentials)
            .then((response) => {
                if (response.data.errors) {
                    rej(response.data.errors);
                }
                res(response.data);
            })
            .catch((err) => {
                rej("Registration failed!");
            });
    })
}

export function getLocalUser() {
    const userStr = localStorage.getItem("user");

    if (!userStr) {
        return null;
    }

    return JSON.parse(userStr);
}

export function logout(store,router) {
    store.commit('logout');
    router.push("/login");
}
