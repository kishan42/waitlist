export function getAssignments() {
    return new Promise((res, rej) => {
        axios.post('/api/assignments')
            .then(response => {
                res(response.data);
            })
            .catch(error => {
                rej(error);
            })
    });
}

export function getAssignmentsData(id) {
    return new Promise((res, rej) => {
        axios.post('/api/assignments/' + id)
            .then(response => {
                res(response.data);
            })
            .catch(error => {
                rej(error);
            })
    });
}

export function saveAssignment(assignment) {
    return new Promise((res, rej) => {
        axios.post('/api/assignments/save', assignment)
            .then(response => {
                if (response.data.errors) {
                    rej(response.data.errors);
                }
                res(response.data);
            })
            .catch((error) => {
                console.log(error);
                rej({error: ["failed saving the assignment!"]});
            })
    });
}
