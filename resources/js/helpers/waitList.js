export function getWaitLists() {
    return new Promise((res, rej) => {
        axios.post('/api/waitlist')
            .then(response => {
                res(response.data);
            })
            .catch(error => {
                rej(error);
            })
    });
}
export function saveWaitList(form,customer) {
    return new Promise((res, rej) => {
        axios.post('/api/waitlist/save',{data:form,customer:customer})
            .then(response => {
                res(response.data);
            })
            .catch(error => {
                rej(error);
            })
    });
}
export function getWaitListData(id) {
    return new Promise((res, rej) => {
        axios.post('/api/waitlist/'+id)
            .then(response => {
                res(response.data);
            })
            .catch(error => {
                rej(error);
            })
    });
}

export function serve(id) {
    return new Promise((res, rej) => {
        axios.post('/api/waitlist/serve/'+id)
            .then(response => {
                res(response.data);
            })
            .catch(error => {
                rej(error);
            })
    });
}

export function getPublicWaitlists(restaurantId) {
    return new Promise((res, rej) => {
        axios.post('/api/waitlist/public/',{restaurant_id:restaurantId})
            .then(response => {
                res(response.data);
            })
            .catch(error => {
                rej(error);
            })
    });
}
export function getLocalWaitingId() {
    const waitingId = localStorage.getItem("waitingId");

    if (!waitingId) {
        return null;
    }

    return JSON.parse(waitingId);
}
