export function getCustomer() {
    return new Promise((res, rej) => {
        axios.post('/api/customers', {})
            .then(response => {
                res(response.data);
            })
            .catch(error => {
                rej(error);
            })
    });
}
export function searchCustomer(data) {
    return new Promise((res, rej) => {
        axios.post('/api/customers/search', data)
            .then(response => {
                res(response.data);
            })
            .catch(error => {
                rej(error);
            })
    });
}
