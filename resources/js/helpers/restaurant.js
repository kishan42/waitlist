export function getRestaurantList() {
    return new Promise((res, rej) => {
        axios.post('/api/restaurant/list')
            .then(response => {
                res(response.data);
            })
            .catch(error => {
                rej(error);
            })
    });
}

export function getLocalRestaurantId() {
    const restaurantId = localStorage.getItem("restaurantId");

    if (!restaurantId) {
        return null;
    }

    return JSON.parse(restaurantId);
}
