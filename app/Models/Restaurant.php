<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $fillable = ['name', 'phone', 'address'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    public function assignments()
    {
        return $this->hasMany(Assignment::class);
    }

    public function waitLists()
    {
        return $this->hasMany(WaitingList::class);
    }
}
