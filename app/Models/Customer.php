<?php

namespace App\Models;

use App\Helpers\RestaurantHelper;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name', 'phone', 'email'];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public static function matchPhone(string $phone)
    {
        return static::where('phone', $phone)->first();
    }

    /**
     *  assign restaurant id at the time of creating
     */
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $model->restaurant_id = RestaurantHelper::getCurrentRestaurantId();
        });
    }
}
