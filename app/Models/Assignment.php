<?php

namespace App\Models;

use App\Helpers\RestaurantHelper;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable = ['name', 'description', 'capacity', 'average_time', 'empty_after', 'customer_id'];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    /**
     *  assign restaurant id at the time of creating
     */
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $model->restaurant_id = RestaurantHelper::getCurrentRestaurantId();
        });
    }
}
