<?php

namespace App\Models;

use App\Helpers\RestaurantHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class WaitingList extends Model
{
    protected $fillable = ['notes', 'serve_in', 'assignment_id', 'quantity'];
    protected $appends = ['reporting_time'];

    public function getReportingTimeAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->timezone('Asia/Kolkata');
    }

    public function getServedAtAttribute()
    {
        if ($this->attributes['served_at']){
            return Carbon::parse($this->attributes['served_at'])->timezone('Asia/Kolkata');
        }
        return null;
    }

    /**
     * get only the records that are active:not yet served
     * or inactive if $flag parameter passed false
     * @param Builder $query
     * @param bool $flag
     * @return Builder
     */
    public function scopeActive(Builder $query, bool $flag = true): Builder
    {
        if ($flag) {
            return $query->where('served_at', null);
        } else {
            return $query->where('served_at', '<>', null);
        }
    }

    /**
     * return records that were created today
     * @param Builder $query
     * @return Builder
     */
    public function scopeToday(Builder $query): Builder
    {
        return $query->whereDate('created_at', Carbon::today());
    }

    /**
     * return records that are cancelled
     * or not cancelled if $flag is false
     * @param Builder $query
     * @param bool $flag
     * @return Builder
     */
    public function scopeCancelled(Builder $query,bool $flag=true): Builder
    {
        return $query->where('cancelled', $flag);

    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function assignment()
    {
        return $this->belongsTo(Assignment::class);
    }

    /**
     *  assign restaurant id at the time of creating
     */
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $model->restaurant_id = RestaurantHelper::getCurrentRestaurantId();
        });
    }
}
