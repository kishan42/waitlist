<?php


namespace App\Helpers;


use App\Models\Restaurant;

class RestaurantHelper
{

    /**
     * get current logged in restaurant's id
     * @return int
     */
    public static function getCurrentRestaurantId():int
    {
        return static::getCurrentRestaurant()->id;
    }

    /**
     * get current logged in restaurant
     * @return Restaurant
     */
    public static function getCurrentRestaurant():Restaurant
    {
        return \Auth::guard('api')->user()->restaurant;
    }
}
