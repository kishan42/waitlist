<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    public function getList()
    {
        $restaurants = Restaurant::all();
        return response()->json([
            'restaurants' => $restaurants,
        ]);
    }

    /**
     * get data for the restaurant
     *
     * @param Restaurant $restaurant
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Restaurant $restaurant, Request $request)
    {
        return response()->json([
            $restaurant => $restaurant
        ]);
    }
}
