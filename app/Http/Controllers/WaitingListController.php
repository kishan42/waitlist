<?php

namespace App\Http\Controllers;


use App\Helpers\RestaurantHelper;
use App\Models\Customer;
use App\Models\Restaurant;
use App\Models\WaitingList;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WaitingListController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except([
            'publicList'
        ]);
    }

    public function get()
    {
        $restaurant = RestaurantHelper::getCurrentRestaurant();
        $waitLists = $restaurant
            ->waitLists()
            ->today()
            ->cancelled(false)
            ->with(['customer', 'assignment'])
            ->get();
        return response()->json(compact('waitLists'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        if ($request->customer['id']) {
            $customer_id = $request->customer['id'];
        } else {
            $customer = Customer::create($request->input('customer'));
            $customer_id = $customer->id;
        }

        if ($request->data['id']) {
            $waitList = WaitingList::find($request->data['id']);
            if (!$waitList) {
                return response()->json([
                    'error' => "waiting list not found",
                ], 404);
            }
            $waitList->customer_id = $customer_id;
            $waitList->update($request->input('data'));

        } else {
            $waitList = new WaitingList($request->input('data'));
            $waitList->customer_id = $customer_id;
            $waitList->save();
        }

        return response()->json(compact('waitList'));
    }


    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(int $id)
    {
        $restaurant_id = RestaurantHelper::getCurrentRestaurantId();
        $waitList = WaitingList::find($id);

        if (!$waitList || $waitList->restaurant_id != $restaurant_id) {
            return response()->json(['error' => 'waitlist not found'], 404);
        }

        $customer = $waitList->customer;

        return response()->json(compact('waitList', 'customer'));
    }

    public function serve($id)
    {
        $restaurant_id = RestaurantHelper::getCurrentRestaurantId();
        $waitList = WaitingList::find($id);

        if (!$waitList || $waitList->restaurant_id != $restaurant_id) {
            return response()->json(['error' => 'waitlist not found'], 404);
        }

        $waitList->served_at = Carbon::now();
        $waitList->save();

        return response()->json([
            "success" => true,
            "served_at" => $waitList->served_at,
        ]);

    }

    public function publicList(Request $request)
    {
        $waitLists = [];
        if ($request->restaurant_id) {
            $restaurant = Restaurant::find($request->restaurant_id);
            if ($restaurant) {
                $waitLists = $restaurant->waitLists()
                    ->active()
                    ->today()
                    ->with(['customer' => function ($query) {
                        $query->select('id','name');
                    }])
                    ->get();
            }
        }

        return response()->json([
            'waitLists' => $waitLists,
        ]);
    }
}
