<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AssignmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function get()
    {
        $assignments = Auth::guard('api')->user()->restaurant->assignments;
        return response()->json(compact('assignments'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:2|max:100',
            'capacity' => 'integer',
            'average_time' => 'integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->messages(),
            ]);
        }

        if ($request->id) {
            $assignment = Assignment::find($request->id);
            if (!$assignment) {
                return response()->json([
                    'error' => "assignment not found",
                ], 404);
            }
            $assignment->update($request->all());
        } else {
            $assignment = Assignment::create($request->all());
        }


        return response()->json([
            'assignment' => $assignment
        ]);
    }

    public function data(int $id)
    {
        $restaurant = Auth::guard('api')->user()->restaurant;
        $assignment = Assignment::find($id);

        if (!$assignment || $assignment->restaurant_id != $restaurant->id) {
            return response()->json(['error' => 'assignment not found'], 404);
        }

        return response()->json(compact('assignment'));
    }
}
