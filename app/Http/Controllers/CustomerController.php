<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\WaitingList;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * return all the customer that belongs to the user logged in
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        $customers = Auth::guard('api')->user()->restaurant->customers;
        return response()->json(compact('customers'));
    }

    /**
     * return customer of specified id if it belongs to the logged in user
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $customer = Auth::gaurd('api')->user()->restaurant()->customer()->where('id', $id)->first();
        return response()->json(compact('customer'));
    }

    public function search(Request $request)
    {
        $customer = Customer::matchPhone($request->phone);
        return response()->json(compact('customer'));
    }

}
