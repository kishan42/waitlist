<?php

Route::group([

    'middleware' => 'api',
    'namespace' => '\App\Http\Controllers',
    'prefix' => 'auth'

], function () {

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::group([
    'middleware' => 'api',
    'prefix' => 'customers',
], function () {
    Route::post('/', 'CustomerController@get');
    Route::post('/search', 'CustomerController@search');
    Route::post('/{id}', 'CustomerController@show');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'assignments',
], function () {
    Route::post('/', 'AssignmentController@get');
    Route::post('/save', 'AssignmentController@save');
    Route::post('/{id}', 'AssignmentController@data');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'waitlist',
], function () {
    Route::post('/', 'WaitingListController@get');
    Route::post('/save', 'WaitingListController@save');
    Route::post('/{id}', 'WaitingListController@data')->where('id','[0-9]+');
    Route::post('/serve/{id}', 'WaitingListController@serve');
});


Route::group([
    'middleware' => 'api',
    'prefix' => 'restaurant',
], function () {
    Route::post('/list', 'RestaurantController@getList');
    Route::post('/{id}', 'RestaurantController@data');
});


/**
 * publicly accessible routes
 */

Route::group([
    'prefix' => 'waitlist',
], function () {
    Route::post('/public', 'WaitingListController@publicList');
});
