<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaitingListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waiting_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id');
            $table->integer('restaurant_id');
            $table->integer('assignment_id');
            $table->text('notes')->nullable();
            $table->integer('serve_in')->default(0); // approximate wait time in minutes
            $table->timestamp('served_at')->nullable()->default(null);
            $table->boolean('cancelled')->default(false);
            $table->integer('quantity'); //no of people to serve
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waiting_lists');
    }
}
